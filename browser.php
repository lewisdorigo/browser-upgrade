<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <title>Please update your browser &mdash; <?php bloginfo('name'); ?></title>

        <?php if(get_browser_template_directory('/favicon')): ?>
        <link rel="shortcut icon" href="<?php get_browser_template_directory_uri('/favicon.ico'); ?>" type="image/x-icon">
        <?php endif; ?>

        <link rel="stylesheet" href="<?php echo get_browser_template_directory_uri('/assets/css/browser.css'); ?>" type="text/css">

        <meta name="robots" content="noindex, nofollow">
    </head>

    <body>
        <div class="site-head" role="banner">
            <div class="wrap">
                <a href="<?php esc_attr(home_url('/')); ?>">
                    <img src="<?php echo get_field('png_logo','option')['url']; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" />
                </a>
            </div>
        </div>

        <div class="site-body" role="main">
            <div class="introduction__container" aria-owns="browser-list">
                <div class="wrap">
                    <h1><?php _e('Your browser is out of date'); ?></h1>
                    <p><?php _e('For a better experience, we recommend upgrading to one of these free, secure, and up-to-date web browsers.'); ?></p>
                </div>
            </div>

            <div class="browser-list__container" id="browser-list">
                <div class="wrap">
                    <ul class="browser-list cf">
                        <li>
                            <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">
                                <span class="browser-list__item browser-list__item--chrome">
                                    <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/chrome.png'); ?>" alt="" aria-labelledby="chrome-label" />

                                    <p id="chrome-label">
                                        <small>Google</small>
                                        <strong>Chrome</strong>
                                    </p>

                                    <span class="button"><?php _e('Download'); ?></span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/firefox/" target="_blank">
                                <span class="browser-list__item browser-list__item--firefox">
                                    <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/firefox.png'); ?>" alt="" aria-labelledby="firefox-label" />

                                    <p id="firefox-label">
                                        <small>Mozilla</small>
                                        <strong>Firefox</strong>
                                    </p>

                                    <span class="button"><?php _e('Download'); ?></span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.microsoft.com/en-us/windows/microsoft-edge" target="_blank">
                                <span class="browser-list__item browser-list__item--edge">
                                    <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/edge.png'); ?>" alt="" aria-labelledby="edge-label" />

                                    <p id="edge-label">
                                        <small>Microsoft</small>
                                        <strong>Edge</strong>
                                    </p>

                                    <span class="button"><?php _e('Download'); ?></span>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.opera.com/" target="_blank">
                                <span class="browser-list__item browser-list__item--opera">
                                    <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/opera.png'); ?>" alt="" aria-labelledby="edge-label" />

                                    <p id="edge-label">
                                        <small>Opera Software</small>
                                        <strong>Opera</strong>
                                    </p>

                                    <span class="button"><?php _e('Download'); ?></span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="reasons-list__container">
                <div class="wrap">
                    <h2><?php _e('Why do I need an updated browser?'); ?></h2>

                    <ul class="reasons-list cf">
                        <li>
                            <div class="media">
                                <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/security.png'); ?>" class="media__img" alt="" />
                                <dl class="media__body">
                                    <dt><?php _e('Security'); ?></dt>
                                    <dd><?php _e('Newer browsers offer better protection against scams, viruses, and other threats.'); ?></dd>
                                </dl>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/compatibility.png'); ?>" class="media__img" alt="" />
                                <dl class="media__body">
                                    <dt><?php _e('Compatibility'); ?></dt>
                                    <dd><?php _e('Websites using new technology will display better, and offer more features.'); ?></dd>
                                </dl>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/speed.png'); ?>" class="media__img" alt="" />
                                <dl class="media__body">
                                    <dt><?php _e('Speed'); ?></dt>
                                    <dd><?php _e('Newer browser can load and display websites faster.'); ?></dd>
                                </dl>
                            </div>
                        </li>

                        <li>
                            <div class="media">
                                <img src="<?php echo get_browser_template_directory_uri('/assets/img/browser/experience.png'); ?>" class="media__img" alt="" />
                                <dl class="media__body">
                                    <dt><?php _e('Experience'); ?></dt>
                                    <dd><?php _e('New features and extensions will give you a more enjoyable experience.'); ?></dd>
                                </dl>
                            </div>
                        </li>
                    </ul>

                    <div class="text--center">
                        <p class="milli"><?php _e('If you can’t, or would rather not upgrade, you can still use our website, but please be aware that since we use modern web technologies, our website might not display correctly.'); ?></p>
                        <a href="<?php echo home_url('/'); ?>" class="button button--continue"><?php _e('Go to website'); ?></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-foot" role="contentinfo">
            <div class="wrap">
                <small>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.<?php echo (get_field('copyright_text','option') ? ' '.get_field('copyright_text','option') : ''); ?></small>
            </div>
        </div>
    </body>
</html>