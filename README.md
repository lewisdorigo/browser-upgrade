This plugin will display a browser-upgrade page. You can include it in your site by using git submodules:

```
git submodule add git@bitbucket.org:madebrave/browser-upgrade.git wp-content/mu-plugins/browser-upgrade/
```

If you’re including into the mu-plugins directory, you’ll need to include a PHP file in the root of the mu-plugins to point to the right file, since Wordpress only includes files from the root.

Something like this will do fine:

```php
<?php
/*
Plugin Name: Browser Upgrde Page
Plugin URI: https://dorigo.co/
Description: Makes a Browser upgrade page
Version: 1.0.0
Author: MadeBrave: Lewis Dorigo
Author URI: https://dorigo.co/
*/
require WPMU_PLUGIN_DIR.'/browser-upgrade/browser-upgrade.php';
```

###Styling the Upgrade Page###

The plugin will check your theme folder for `browser.php`, and use that for markup, if it exists. Additionally, you can override images and stylesheets without changing the markup. It’ll check your theme directory for images in `/assets/img/browser/`, and for CSS in `/assets/css/browser.css`.

###Browser Compatibility###

By default, the following browsers will be redirected to the upgrade page:

* Chrome 1–42
* Firefox 1–38
* Opera 1–29
* Safari 1–8
* IE 1-9

You can use the `browser_upgrade_browsers` filter to modify this list in your theme. For example, if you want to change Chrome’s versions to redirect from version 1–49, you can do the following:

```php
function change_browser_compatibility($testStrings) {
    $testStrings['chrome'] = 'Chrome\/([1-9]\.|[1-4][0-9]\.)';
    return $testStrings;
}
add_filter('browser_upgrade_browsers', 'change_browser_compatibility', 10, 1);
```

It’s important to note that regex doesn’t can’t match 1–49, so the regex must match [1–9] *or* [1-4][0-9].

The default test strings are:

```php
$testStrings = array (
    'chrome'    => 'Chrome\/([1-9]\.|[1-3][0-9]\.|[4][0-2]\.)',
    'firefox'   => 'Firefox\/([1-9]\.|[1-2][0-9]\.|[3][0-8]\.)',
    'opera-old' => 'Opera\/([1-8]\.|9\.[0-7]|9\.8(?!.* Version)|[0-9]\..* Version\/([0-9]\.|[0-2][0-9]\.))',
    'opera'     => 'OPR\/([1-9]\.|[1-2][0-9]\.)',
    'safari'    => 'Version\/([1-8]\.).* Safari',
    'msie'      => 'MSIE ([1-9]\.|10\.)',
);
```

Opera’s User Agent strings are weird. From versions 1-9, it’s simple to match it, but from versions 10+, they retained `Opera/9.80` in the string, appending `Version/{$version_number}`. Then, when they switched to using Webkit (then Blink) as the rendering engine, they moved to using `OPR\{$version}`.

Opera using the old `Opera/{$version_number}` User Agent is specified under `opera-old`, and newer versions using `OPR\{$version_number}` is under `opera`.