<?php
/*
Plugin Name: Browser Upgrde Page
Plugin URI: https://bitbucket.org/lewisdorigo/gift-wrapper
Description: Makes a Browser upgrade page
Version: 1.0.0
Author: Lewis Dorigo
Author URI: https://dorigo.co/
*/

function browserupgrade_init() {
	global $wp_rewrite;

	$rewrite_rules = get_option('rewrite_rules');

	add_filter('query_vars', create_function('$qv', '$qv[] = "browser-upgrade"; return $qv;'));
	add_rewrite_rule('browser-upgrade$', $wp_rewrite->index . '?browser-upgrade=1', 'top');

	// flush rewrite rules if ours is missing
	if (!isset($rewrite_rules['browser-upgrade$'])) {
		flush_rewrite_rules(false);
	}
}
add_action('init', 'browserupgrade_init');

function is_browser_upgrade() {
	return (bool) get_query_var('browser-upgrade');
}

function browserupgrade_redirect() {
    if(is_browser_upgrade() || isset($_COOKIE['browser-upgrade'])) {
        return false;
    }

    $testStrings = array (
        'chrome'    => 'Chrome\/([1-9]\.|[1-3][0-9]\.|[4][0-2]\.)',
        'firefox'   => 'Firefox\/([1-9]\.|[1-2][0-9]\.|[3][0-8]\.)',
        'opera-old' => 'Opera\/([1-8]\.|9\.[0-7]|9\.8(?!.* Version)|[0-9]\..* Version\/([0-9]\.|[0-2][0-9]\.))',
        'opera'     => 'OPR\/([1-9]\.|[1-2][0-9]\.)',
        'safari'    => 'Version\/([1-8]\.).* Safari',
        'msie'      => 'MSIE ([1-9]\.|10\.)',
    );

    $testStrings = apply_filters('browser_upgrade_browsers', $testStrings);

    $test = '/('.implode('|', $testStrings).')/';

    if(preg_match($test, $_SERVER['HTTP_USER_AGENT'])) {
        return true;
    } else {
        return false;
    }
}

function get_browser_template_directory($file = '/browser.php') {
    if(file_exists(get_stylesheet_directory().$file)) {
    	return get_stylesheet_directory().$file;
	} elseif (file_exists(get_template_directory().$file)) {
    	return get_template_directory().$file;
	} else {
    	return dirname(__FILE__).$file;
    }
}

function get_browser_template_directory_uri($file = '/browser.php') {
    if(file_exists(get_stylesheet_directory().$file)) {
    	return get_stylesheet_directory_uri().$file;
	} elseif (file_exists(get_template_directory().$file)) {
    	return get_template_directory_uri().$file;
	} else {
    	return plugins_url($file,__FILE__);
    }
}

function browserupgrade_page_redirect() {
	if(browserupgrade_redirect()) {
    	wp_redirect(home_url('/browser-upgrade/'));
    	exit();
	}
}
//add_action('template_redirect', 'browserupgrade_page_redirect', 7);

function browserupgrade_template_redirect() {
	if(is_browser_upgrade()) {
		do_action('do_browser-upgrade');
		exit;
	}
}
add_action('template_redirect', 'browserupgrade_template_redirect', 8);

function browser_ugprade_do() {
    $file = get_browser_template_directory();

    require_once($file);
}
add_action('do_browser-upgrade', 'browser_ugprade_do',9999);

function set_browser_upgrade_cookie() {
    setcookie("browser-upgrade", 'true', (time()+(3600*24*31)), '/');
}
add_action('do_browser-upgrade', 'set_browser_upgrade_cookie',1);

function browser_upgrade_to_robots() {
    echo  "User-agent: *";
    echo  "\nDisallow: /browser-upgrade";
    echo  "\n\n";
}
add_action('do_robotstxt', 'browser_upgrade_to_robots');